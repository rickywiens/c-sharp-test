using System;

namespace ExampleLib2
{
    public static class Calculator2
    {
        public static int Add(int first, int second)
        {
            return first + second;
        }

        public static int Multiply(int first, int second)
        {
            return first * second;
        }

        public static int Divide(int first, int second)
        {
            return first / second;
        }

        public static int Mod(int first, int second)
        {
            return first % second;
        }
    }
}