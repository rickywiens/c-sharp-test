using NUnit.Framework;

namespace ExampleLib2.UnitTests
{
    public class Tests
    {
        [Test]
        public void TestAdding()
        {
            Assert.That(Calculator2.Add(2, 2), Is.EqualTo(4));
        }

        public void TestMultiplying()
        {
            Assert.That(Calculator2.Multiply(2, 2), Is.EqualTo(4));
        }

        public void TestDividing()
        {
            Assert.That(Calculator2.Divide(10, 2), Is.EqualTo(5));
        }
    }
}