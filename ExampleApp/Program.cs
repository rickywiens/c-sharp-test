﻿using System;

namespace ExampleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var version = assembly.GetName().Version;
            Console.WriteLine($"{version}");
        }
    }
}
